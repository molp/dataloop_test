class Data:
    def __init__(self, id=None, name=None, metadata=None):
        self.id = id
        self.name = name
        self.metadata = metadata or {}

    @classmethod
    def from_dict(cls, data_dict):
        return cls(**data_dict)

    def to_dict(self):
        """
            Convert the `Data` object to a dictionary representation.
            Returns:
                dict: A dictionary representing the `Data` object.
        """
        return {
            "id": self.id,
            "name": self.name,
            "metadata": self.metadata
        }

    @property
    def size(self):
        """get size in metadata.system.size"""
        get_size = self.metadata.get("system", {}).get("size", None)

        if not get_size:
            return None

        return int(get_size)

    @property
    def height(self):
        """get height from system.height"""
        return self.metadata.get("system", {}).get("height", None)

    @height.setter
    def height(self, value):
        """set value to systeme.height"""
        system = self.metadata.get("system", {})
        system['height'] = value

data = {
    "id": "1",
    "name": "first",
    "metadata": {
        "system": {
            "size": 10.7
        },
        "user": {
            "batch": 10
        }
    }
}

if __name__ == "__main__":
    # load from dict
    my_inst_1 = Data.from_dict(data)

    # Save to dict
    my_dict = my_inst_1.to_dict()

    # load from inputs
    my_inst_2 = Data(name="my")

    # # reflect inner value
    print(my_inst_1.size)  # should print 10
    #
    # # default values
    my_inst_1.height = 100
    print(my_inst_1.height)  # should set a default value of 100 in metadata.system.height
    print(my_inst_1.to_dict()['metadata']['system']['height'])  # should print the default value

    # autocomplete - should complete to metadata
